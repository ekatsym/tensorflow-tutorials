#!/bin/bash

docker build\
    --build-arg user=$USER\
    --build-arg uid=$(id -u)\
    --build-arg gid=$(id -g)\
    --no-cache=true\
    -t ekatsym/tensorflow .
