#!/bin/bash
docker run -itd --name tensorflow-tutorials\
    --net host\
    -v $HOME/.Xauthority:$HOME/.Xauthority:rw\
    -v $PWD:$HOME/tensorflow\
    -e DISPLAY=$DISPLAY\
    -e TERM=$TERM\
    --gpus all\
    ekatsym/tensorflow
