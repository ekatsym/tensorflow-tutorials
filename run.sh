#!/bin/bash
docker run -itd --name tensorflow-tutorials\
    -v /tmp/.X11-unix:/tmp/.X11-unix\
    -v $XAUTHORITY:$HOME/.Xauthority:rw\
    -e XAUTHORITY=$HOME/.Xauthority\
    -v $PWD:$HOME/tensorflow\
    -e DISPLAY=$DISPLAY\
    -e TERM=$TERM\
    --gpus all\
    ekatsym/tensorflow
